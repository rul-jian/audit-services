create table AUDIT_RECORD (
	AUDIT_ID number(5) primary key,
	OPERATION char(1) not null,
	OLD_VALUE varchar2(100),
	NEW_VALUE varchar2(100),
	CREATE_TS timestamp
);

create sequence AUDIT_RECORD_SEQ
minvalue 1;