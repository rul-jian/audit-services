package com.bibao.audit.rest.config;

import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import io.swagger.jaxrs.listing.SwaggerSerializers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Value("${server.servlet.contextPath:/}")
    private String serverContextPath;

    @Value("${spring.jersey.application-path}")
    private String jerseyApiPath;

    @Autowired
    JerseyConfig jerseyConfig;

    /*Swagger Annotation Configuration for Jersey Endpoints
     *
     * The Json can be seen on this link- {url}:{port}/{jersey-application-path}/swagger.json
     *
     * */
    @Bean
    public BeanConfig jerseySwaggerBean() {
        jerseyConfig.register(ApiListingResource.class);
        jerseyConfig.register(SwaggerSerializers.class);

        BeanConfig config = new BeanConfig();
        config.setTitle("Audit Services Swagger UI");
        config.setVersion("Release 1.0.0");
        config.setSchemes(new String[]{"https"});
        config.setBasePath(this.jerseyApiPath);
        config.setResourcePackage("com.bibao.audit.rest");
        config.setPrettyPrint(true);
        config.setScan(true);

        return config;
    }

    /* Swagger configuration bean for Spring mvc endpoints which also includes actuator endpoints.
     *
     * UI can be see going through- {url}:{port}/{context-path}/swagger-ui.html
     *
     * */

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .pathMapping(serverContextPath)
                .apiInfo(apiInfo());
    }

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "Audit Services in Bibao Inc",
                "Audit Services Swagger Api",
                "",
                "",
                new Contact("Bibao Inc", "", "bibaoinc@gmail.com"),
                "",
                "",
                new ArrayList<>());
    }
    
}
