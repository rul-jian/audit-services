package com.bibao.audit.rest.contract;

import com.bibao.audit.model.AuditRecord;

public class AuditRecordResponse {
	private AuditRecord record;
	private String message;
	
	public AuditRecord getRecord() {
		return record;
	}
	public void setRecord(AuditRecord record) {
		this.record = record;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	
}
