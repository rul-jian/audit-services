package com.bibao.audit.rest.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;
import org.springframework.integration.handler.LoggingHandler;
import org.springframework.integration.jms.dsl.Jms;
import org.springframework.integration.json.ObjectToJsonTransformer;
import org.springframework.integration.json.ObjectToJsonTransformer.ResultType;
import org.springframework.integration.support.json.Jackson2JsonObjectMapper;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.messaging.MessageChannel;

import com.bibao.audit.publisher.config.AuditPublisherConfig;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@EnableJms
@EnableIntegration
@IntegrationComponentScan
@Import(value = { AuditPublisherConfig.class, JmsConfig.class, JerseyConfig.class})
public class AuditRestConfig {
	private static final String LOGGER = "audit.event";
	
	@Autowired
	private JmsConnectionProps jmsProps;
	
	@Autowired
	private ActiveMQConnectionFactory connectionFactory;
 
	@Bean
	public ObjectMapper mapper() {
		ObjectMapper mapper = Jackson2ObjectMapperBuilder
						.json()
						.findModulesViaServiceLoader(true)
						.serializationInclusion(Include.NON_ABSENT)
						.build();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		return mapper;
	}
	
	@Bean
	public Jackson2JsonObjectMapper jackson2JsonObjectMapper(ObjectMapper mapper) {
		return new Jackson2JsonObjectMapper(mapper);
	}
	
	@Bean
	public MessagingTemplate auditMessagingTemplate() {
		MessagingTemplate template = new MessagingTemplate();
		template.setSendTimeout(0); 
		template.setDefaultChannel(auditMessageChannel());
		return template;
	}
	
	@Bean
	public MessageChannel auditMessageChannel() {
		return new DirectChannel();		
	}	
	
	@Bean
	public ObjectToJsonTransformer transformer(Jackson2JsonObjectMapper mapper) {
		return new ObjectToJsonTransformer(mapper, ResultType.STRING);
	}
	
	@Bean
	public IntegrationFlow auditFlow(ObjectToJsonTransformer transformer) {
		return IntegrationFlows
					.from(auditMessageChannel())
					.log(LoggingHandler.Level.INFO, LOGGER, m -> "Publishing to audit: " + m.getPayload())
					.transform(transformer)
					.handle(Jms.outboundAdapter(connectionFactory)
							.destination(jmsProps.getAuditQueue()))
					.get();
	}
}
