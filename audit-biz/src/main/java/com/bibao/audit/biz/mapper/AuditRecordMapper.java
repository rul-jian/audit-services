package com.bibao.audit.biz.mapper;

import java.time.LocalDateTime;

import com.bibao.audit.model.AuditRecord;
import com.bibao.audit.persistence.entity.AuditRecordEntity;

public class AuditRecordMapper {
	
	public static AuditRecordEntity mapToAuditRecordEntity(AuditRecord record) {
		AuditRecordEntity entity = new AuditRecordEntity();
		entity.setOldValue(record.getOldValue());
		entity.setNewValue(record.getNewValue());
		entity.setOperation(record.getOperation());
		entity.setCreateTimestamp(LocalDateTime.now());
		return entity;
	}
}
