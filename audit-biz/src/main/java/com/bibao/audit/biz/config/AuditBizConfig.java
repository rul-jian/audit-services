package com.bibao.audit.biz.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import com.bibao.audit.persistence.config.JPAConfig;

@Configuration
@ComponentScan("com.bibao.audit.biz")
@Import(value = { JPAConfig.class})
public class AuditBizConfig {

}
