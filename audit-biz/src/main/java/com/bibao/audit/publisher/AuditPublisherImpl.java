package com.bibao.audit.publisher;

import java.util.function.Supplier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.core.MessagingTemplate;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

@Component
public class AuditPublisherImpl implements AuditPublisher {
	@Autowired
	private MessagingTemplate template;
	
	@Override
	public void publishMessage(Supplier<Message<?>> msgSupplier) {
		template.send(msgSupplier.get());
	}

}
