package com.bibao.audit.publisher.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;


@Configuration
@ComponentScan("com.bibao.audit.publisher")
public class AuditPublisherConfig {

}
