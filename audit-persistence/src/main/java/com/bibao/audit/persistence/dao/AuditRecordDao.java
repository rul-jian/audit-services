package com.bibao.audit.persistence.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.bibao.audit.persistence.entity.AuditRecordEntity;

public interface AuditRecordDao extends JpaRepository<AuditRecordEntity, Integer> {

}
