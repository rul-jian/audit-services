package com.bibao.audit.persistence.dao;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.data.jpa.JpaRepositoriesAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.bibao.audit.persistence.config.JPAConfig;
import com.bibao.audit.persistence.entity.AuditRecordEntity;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = JPAConfig.class)
@EnableAutoConfiguration(exclude = JpaRepositoriesAutoConfiguration.class)
public class AuditRecordDaoTest {
	@Autowired
	private AuditRecordDao dao;
	
	@BeforeEach
	public void setUp() {
		List<AuditRecordEntity> entities = new ArrayList<>();
		entities.add(createEntity("U", "false", "true"));
		entities.add(createEntity("U", "100", "200"));
		entities.add(createEntity("U", "New York", "New Jersey"));
		entities.add(createEntity("I", null, "100 Test Dr"));
		entities.add(createEntity("I", null, "58000"));
		dao.saveAll(entities);
	}
	
	@AfterEach
	public void tearDown() {
		dao.deleteAll();
	}
	
	@Test
	public void testSavedRecords() {
		assertEquals(5, dao.count());
	}
	
	private AuditRecordEntity createEntity(String operation, String oldValue, String newValue) {
		AuditRecordEntity entity = new AuditRecordEntity();
		entity.setOperation(operation);
		entity.setOldValue(oldValue);
		entity.setNewValue(newValue);
		return entity;
	}
}
