package com.bibao.audit.batch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

import com.bibao.audit.batch.config.AuditBatchConfig;

@SpringBootApplication
@Import(AuditBatchConfig.class)
public class AuditBatchApp {
	
	public static void main(String[] args) {
		SpringApplication.run(AuditBatchApp.class, args);
	}
}
