package com.bibao.audit.batch.config;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
public class JmsConfig {

	@Autowired
	private JmsConnectionProps jmsProps;

	@Bean(name="connectionFactory")
	public ActiveMQConnectionFactory connectionFactory(){
	    ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
	    connectionFactory.setBrokerURL(jmsProps.getUrl());
	    connectionFactory.setPassword(jmsProps.getUsername());
	    connectionFactory.setUserName(jmsProps.getPassword());
	    return connectionFactory;
	}
	
	@Bean
	public JmsTemplate jmsTemplate(ActiveMQConnectionFactory connectionFactory) {
		return new JmsTemplate(connectionFactory);
	}

	@Bean
	public DefaultMessageListenerContainer myJmsListenerContainerFactory(ActiveMQConnectionFactory connectionFactory) {
		DefaultMessageListenerContainer container = new DefaultMessageListenerContainer();
		container.setDestinationName(jmsProps.getAuditQueue());
		container.setConnectionFactory(connectionFactory);
		container.setConcurrentConsumers(1);
		container.setMaxConcurrentConsumers(5);
		return container;
	}	
	
}
