package com.bibao.audit.batch.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "mq.jms")
public class JmsConnectionProps {	
	private String url;
	private String username;
	private String password;
	private String auditQueue;
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAuditQueue() {
		return auditQueue;
	}
	public void setAuditQueue(String auditQueue) {
		this.auditQueue = auditQueue;
	}
}
